### Detalhes

Como a linguagem não foi definida se iria ser necessário escrever tudo em inglês, eu fiz da forma que ficou legível pra ler.

### Observações

Não vou ter tempo para terminar os testes de integrações e end-to-end, caso queiram eu posso fazer. Mas o prazo para entrega é amanhã 30/05/2021, e eu tenho um compromisso hoje a noite 29/05/2021.
Caso queiram posso fazer sim os testes de integrações no domingo, senão tudo bem.

Tenho um bom conhecimento em PHP principalmente, e estou estudando sobre DDD recentemente, por isso podem ter alguns erro, ou imprecisões sobre as camadas.

Conheço Javascript, Python, HTML, CSS, React, já trabalhei um pouco com VueJS, e estou estudando sobre o TypeScript com NodeJS

Tentei fazer o melhor que consegui em 2 dias.

Seguem os links:

[Github](https://github.com/paulo123araujo)

[LinkedIn](https://www.linkedin.com/in/paulo123araujo/)

A documentação da api eu fiz pelo share do postman:
[Documentação API da aplicação](https://www.getpostman.com/collections/233ed3d268048108ff0d)

Estou aberto para conversarmos, através do:

**E-mail**: paulofelipe_jau7654@hotmail.com

**WhatsApp**: 14 99811-6746