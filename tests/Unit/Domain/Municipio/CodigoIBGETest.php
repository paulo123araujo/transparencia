<?php

namespace Intec\TransparenciaViagensServico\Test\Unit\Domain\Municipio;

use Intec\TransparenciaViagensServico\Domain\Municipio\CodigoIBGE;
use PHPUnit\Framework\TestCase;

class CodigoIBGETest extends TestCase
{
    /**
     * @test
     */
    public function shouldCreateObjectCorrectly()
    {
        $codigoIBGE = CodigoIBGE::new("123456");
        $this->assertInstanceOf(CodigoIBGE::class, $codigoIBGE);
        $this->assertEquals("123456", $codigoIBGE->codigoIBGE());
    }
}