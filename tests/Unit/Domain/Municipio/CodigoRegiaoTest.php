<?php

namespace Intec\TransparenciaViagensServico\Test\Unit\Domain\Municipio;

use Intec\TransparenciaViagensServico\Domain\Municipio\CodigoRegiao;
use PHPUnit\Framework\TestCase;

class CodigoRegiaoTest extends TestCase
{
    /**
     * @test
     */
    public function shouldCreateObjectCorrectly()
    {
        $codigoRegiao = CodigoRegiao::new("123456");
        $this->assertInstanceOf(CodigoRegiao::class, $codigoRegiao);
        $this->assertEquals("123456", $codigoRegiao->codigoRegiao());
    }
}