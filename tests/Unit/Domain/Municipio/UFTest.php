<?php

namespace Intec\TransparenciaViagensServico\Test\Unit\Domain\Municipio;

use DomainException;
use Intec\TransparenciaViagensServico\Domain\Municipio\UF;
use PHPUnit\Framework\TestCase;

class UFTest extends TestCase
{
    /**
     * @test
     */
    public function shouldCreateUFCorrectly()
    {
        $uf = UF::new("SP", "SÃO PAULO");
        $this->assertInstanceOf(UF::class, $uf);
        $this->assertEquals("SP", $uf->sigla());
        $this->assertEquals("SÃO PAULO", $uf->nome());
    }

    /**
     * @test
     */
    public function shouldThrowAnExceptionIfInitialIsNotValid()
    {
        $this->expectException(DomainException::class);
        $this->expectExceptionMessage("Estado inválido.");
        UF::new("SS", "SÃO PAULO");
    }

    /**
     * @test
     */
    public function shouldThrowAnExceptionIfNameDoesNotMatch()
    {
        $this->expectException(DomainException::class);
        $this->expectExceptionMessage("Estado inválido.");
        UF::new("SP", "SANTA CATARINA");
    }
}