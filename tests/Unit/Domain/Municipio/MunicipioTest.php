<?php

namespace Intec\TransparenciaViagensServico\Test\Unit\Domain\Municipio;

use PHPUnit\Framework\TestCase;

class MunicipioTest extends TestCase
{
    /**
     * @test
     */
    public function shouldBeEqualsIfMunicipioIsTheSame()
    {
        $municipio1 = MunicipioBuilder::aInstance()->build();
        $municipio2 = MunicipioBuilder::aInstance()->build();

        $this->assertTrue($municipio1->isEquals($municipio2));
    }
}