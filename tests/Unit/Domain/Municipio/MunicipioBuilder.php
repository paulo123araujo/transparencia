<?php

namespace Intec\TransparenciaViagensServico\Test\Unit\Domain\Municipio;

use Intec\TransparenciaViagensServico\Domain\Municipio\{
    CodigoIBGE,
    CodigoRegiao,
    Municipio,
    NomeIBGE,
    NomeRegiao,
    Pais,
    UF
};

class MunicipioBuilder
{
    private CodigoIBGE $codigoIBGE;
    private CodigoRegiao $codigoRegiao;
    private NomeIBGE $nomeIBGE;
    private NomeRegiao $nomeRegiao;
    private Pais $pais;
    private UF $uf;

    public function __construct()
    {
        $this->codigoIBGE = CodigoIBGE::new("1234");
        $this->codigoRegiao = CodigoRegiao::new("1234");
        $this->nomeIBGE = NomeIBGE::new("1234");
        $this->nomeRegiao = NomeRegiao::new("1234");
        $this->pais = Pais::new("BRASIL");
        $this->uf = UF::new("SP", "SÃO PAULO");
    }

    public static function aInstance(): MunicipioBuilder
    {
        return new self();
    }

    public function build(): Municipio
    {
        return new Municipio(
            $this->codigoIBGE,
            $this->nomeIBGE,
            $this->codigoRegiao,
            $this->nomeRegiao,
            $this->pais,
            $this->uf,
            null
        );
    }
}