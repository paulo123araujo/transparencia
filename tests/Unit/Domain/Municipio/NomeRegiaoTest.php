<?php

namespace Intec\TransparenciaViagensServico\Test\Unit\Domain\Municipio;

use Intec\TransparenciaViagensServico\Domain\Municipio\NomeRegiao;
use PHPUnit\Framework\TestCase;

class NomeRegiaoTest extends TestCase
{
    /**
     * @test
     */
    public function shouldCreateObjectCorrectly()
    {
        $nomeRegiao = NomeRegiao::new("123456");
        $this->assertInstanceOf(NomeRegiao::class, $nomeRegiao);
        $this->assertEquals("123456", $nomeRegiao->nomeRegiao());
    }
}