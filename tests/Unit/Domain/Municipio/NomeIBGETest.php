<?php

namespace Intec\TransparenciaViagensServico\Test\Unit\Domain\Municipio;

use Intec\TransparenciaViagensServico\Domain\Municipio\NomeIBGE;
use PHPUnit\Framework\TestCase;

class NomeIBGETest extends TestCase
{
    /**
     * @test
     */
    public function shouldCreateObjectCorrectly()
    {
        $nomeIBGE = NomeIBGE::new("123456");
        $this->assertInstanceOf(NomeIBGE::class, $nomeIBGE);
        $this->assertEquals("123456", $nomeIBGE->nomeIBGE());
    }
}