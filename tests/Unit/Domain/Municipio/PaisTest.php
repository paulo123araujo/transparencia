<?php

namespace Intec\TransparenciaViagensServico\Test\Unit\Domain\Municipio;

use DomainException;
use Intec\TransparenciaViagensServico\Domain\Municipio\Pais;
use PHPUnit\Framework\TestCase;

class PaisTest extends TestCase
{
    /**
     * @test
     */
    public function shouldThrowAnExceptionIfIsNotBrazil()
    {
        $this->expectException(DomainException::class);
        $this->expectExceptionMessage("O país precisa ser o Brasil.");
        Pais::new("PARAGUAY");
    }

    /**
     * @test
     */
    public function shouldCreateObjectCorrectly()
    {
        $pais = Pais::new("BRASIL");
        $this->assertInstanceOf(Pais::class, $pais);
        $this->assertEquals("BRASIL", $pais->pais());
    }
}