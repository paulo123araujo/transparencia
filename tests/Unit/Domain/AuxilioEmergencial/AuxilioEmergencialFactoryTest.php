<?php

namespace Intec\TransparenciaViagensServico\Test\Unit\Domain\AuxilioEmergencial;

use DateTimeImmutable;
use Intec\TransparenciaViagensServico\Domain\AuxilioEmergencial\AuxilioEmergencial;
use Intec\TransparenciaViagensServico\Domain\AuxilioEmergencial\AuxilioEmergencialFactory;
use PHPUnit\Framework\TestCase;

class AuxilioEmergencialFactoryTest extends TestCase
{
    /**
     * @test
     */
    public function shouldReturnCorrectObject()
    {
        $auxilio = AuxilioEmergencialFactory::create(
            "1234",
            "1234",
            "1234",
            "1234",
            "BRASIL",
            "SP",
            "SÃO PAULO",
            1000,
            50,
            new DateTimeImmutable("2020-05-13"),
            1
        );

        $this->assertInstanceOf(AuxilioEmergencial::class, $auxilio);
    }
}
