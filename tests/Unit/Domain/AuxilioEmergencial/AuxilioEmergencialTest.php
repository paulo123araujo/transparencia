<?php

namespace Intec\TransparenciaViagensServico\Test\Unit\Domain\AuxilioEmergencial;

use DateTimeImmutable;
use Intec\TransparenciaViagensServico\Domain\AuxilioEmergencial\AuxilioEmergencial;
use Intec\TransparenciaViagensServico\Test\Unit\Domain\Municipio\MunicipioBuilder;
use PHPUnit\Framework\TestCase;

class AuxilioEmergencialTest extends TestCase
{
    /**
     * @test
     */
    public function shouldCreateAuxilioEmergencialCorrectly()
    {
        $auxilio = new AuxilioEmergencial(
            MunicipioBuilder::aInstance()->build(),
            1000.00,
            50,
            new DateTimeImmutable("2020-05-13")
        );

        $this->assertInstanceOf(AuxilioEmergencial::class, $auxilio);
        $this->assertEquals(1000.00, $auxilio->valor());
        $this->assertEquals(50, $auxilio->quantidadeBeneficiados());
        $this->assertEquals("2020-05-13", $auxilio->dataReferencia()->format("Y-m-d"));
    }
}
