<?php

namespace Intec\TransparenciaViagensServico\Test\Unit\Domain\BPC;

use DateTimeImmutable;
use Intec\TransparenciaViagensServico\Domain\BPC\BPC;
use Intec\TransparenciaViagensServico\Domain\BPC\BPCFactory;
use PHPUnit\Framework\TestCase;

class BPCFactoryTest extends TestCase
{
    /**
     * @test
     */
    public function shouldReturnCorrectObject()
    {
        $bpc = BPCFactory::create(
            "1234",
            "1234",
            "1234",
            "1234",
            "BRASIL",
            "SP",
            "SÃO PAULO",
            1000,
            50,
            new DateTimeImmutable("2020-05-13"),
            1
        );

        $this->assertInstanceOf(BPC::class, $bpc);
    }
}