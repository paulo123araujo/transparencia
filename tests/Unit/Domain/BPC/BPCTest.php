<?php

namespace Intec\TransparenciaViagensServico\Test\Unit\Domain\BPC;

use DateTimeImmutable;
use Intec\TransparenciaViagensServico\Domain\BPC\BPC;
use Intec\TransparenciaViagensServico\Test\Unit\Domain\Municipio\MunicipioBuilder;
use PHPUnit\Framework\TestCase;

class BPCTest extends TestCase
{
    /**
     * @test
     */
    public function shouldCreateBPCCorrectly()
    {
        $bpc = new BPC(
            MunicipioBuilder::aInstance()->build(),
            1000.00,
            50,
            new DateTimeImmutable("2020-05-13")
        );

        $this->assertInstanceOf(BPC::class, $bpc);
        $this->assertEquals(1000.00, $bpc->valor());
        $this->assertEquals(50, $bpc->quantidadeBeneficiados());
        $this->assertEquals("2020-05-13", $bpc->dataReferencia()->format("Y-m-d"));
    }
}