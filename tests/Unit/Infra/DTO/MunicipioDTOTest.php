<?php

namespace Intec\TransparenciaViagensServico\Test\Unit\Infra\DTO;

use Intec\TransparenciaViagensServico\Domain\Municipio\Municipio;
use Intec\TransparenciaViagensServico\Infra\DTO\MunicipioDTO;
use PHPUnit\Framework\TestCase;

class MunicipioDTOTest extends TestCase
{
    /**
     * @test
     */
    public function shouldReturnCorrectObjectFromDatabaseMethod()
    {
        $data = [
            "codigoIBGE" => "1234",
            "nomeIBGE" => "1234",
            "codigoRegiao" => "1234",
            "nomeRegiao" => "1234",
            "pais" => "BRASIL",
            "ufSigla" => "SP",
            "ufNome" => "SÃO PAULO",
            "id" => 1
        ];

        $tested = MunicipioDTO::fromDatabase($data);
        $this->assertInstanceOf(Municipio::class, $tested);
        $this->assertMunicipioValues($data, $tested);
    }

    private function assertMunicipioValues(array $data, Municipio $tested): void
    {
        $this->assertEquals($data["codigoIBGE"], $tested->codigoIBGE()->codigoIBGE());
        $this->assertEquals($data["nomeIBGE"], $tested->nomeIBGE()->nomeIBGE());
        $this->assertEquals($data["codigoRegiao"], $tested->codigoRegiao()->codigoRegiao());
        $this->assertEquals($data["nomeRegiao"], $tested->nomeRegiao()->nomeRegiao());
        $this->assertEquals($data["pais"], $tested->pais()->pais());
        $this->assertEquals($data["id"], $tested->id());
        $this->assertEquals($data["ufSigla"], $tested->uf()->sigla());
        $this->assertEquals($data["ufNome"], $tested->uf()->nome());
    }
}