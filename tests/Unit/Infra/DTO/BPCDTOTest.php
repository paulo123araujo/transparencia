<?php

namespace Intec\TransparenciaViagensServico\Test\Unit\Infra\DTO;

use Intec\TransparenciaViagensServico\Domain\BPC\BPC;
use Intec\TransparenciaViagensServico\Infra\DTO\BPCDTO;
use PHPUnit\Framework\TestCase;

class BPCDTOTest extends TestCase
{
    /**
     * @test
     */
    public function shouldReturnCorrectDataFromDatabaseMethod()
    {
        $data = [
            "id" => 1,
            "codigoIBGE" => "1234",
            "codigoRegiao" => "1234",
            "nomeIBGE" => "1234",
            "nomeRegiao" => "1234",
            "pais" => "BRASIL",
            "ufSigla" => "SP",
            "ufNome" => "SÃO PAULO",
            "valor" => 1000,
            "quantidadeBeneficiados" => 50,
            "dataReferencia" => "2020-05-13",
            "municipio_id" => 1
        ];

        $tested = BPCDTO::fromDatabase($data);
        $this->assertInstanceOf(BPC::class, $tested);
        $this->assertValuesFromAuxilio($data, $tested);
    }

    private function assertValuesFromAuxilio(
        array $expected,
        BPC $tested
    ): void {
        $this->assertEquals($expected["codigoIBGE"], $tested->municipio()->codigoIBGE()->codigoIBGE());
        $this->assertEquals($expected["codigoRegiao"], $tested->municipio()->codigoRegiao()->codigoRegiao());
        $this->assertEquals($expected["nomeRegiao"], $tested->municipio()->nomeRegiao()->nomeRegiao());
        $this->assertEquals($expected["nomeIBGE"], $tested->municipio()->nomeIBGE()->nomeIBGE());
        $this->assertEquals($expected["pais"], $tested->municipio()->pais()->pais());
        $this->assertEquals($expected["ufSigla"], $tested->municipio()->uf()->sigla());
        $this->assertEquals($expected["ufNome"], $tested->municipio()->uf()->nome());
        $this->assertEquals($expected["municipio_id"], $tested->municipio()->id());

        $this->assertEquals($expected["valor"], $tested->valor());
        $this->assertEquals($expected["quantidadeBeneficiados"], $tested->quantidadeBeneficiados());
        $this->assertEquals($expected["dataReferencia"], $tested->dataReferencia()->format("Y-m-d"));
    }

    private function assertValuesFromAuxilioAPI(
        array $expected,
        BPC $tested
    ): void {
        $this->assertEquals($expected["municipio"]->codigoIBGE, $tested->municipio()->codigoIBGE()->codigoIBGE());
        $this->assertEquals($expected["municipio"]->codigoRegiao, $tested->municipio()->codigoRegiao()->codigoRegiao());
        $this->assertEquals($expected["municipio"]->nomeRegiao, $tested->municipio()->nomeRegiao()->nomeRegiao());
        $this->assertEquals($expected["municipio"]->nomeIBGE, $tested->municipio()->nomeIBGE()->nomeIBGE());
        $this->assertEquals($expected["municipio"]->pais, $tested->municipio()->pais()->pais());
        $this->assertEquals($expected["municipio"]->uf->sigla, $tested->municipio()->uf()->sigla());
        $this->assertEquals($expected["municipio"]->uf->nome, $tested->municipio()->uf()->nome());
        $this->assertNull($tested->municipio()->id());

        $this->assertEquals($expected["valor"], $tested->valor());
        $this->assertEquals($expected["quantidadeBeneficiados"], $tested->quantidadeBeneficiados());
        $this->assertEquals($expected["dataReferencia"], $tested->dataReferencia()->format("Y-m-d"));
    }

    /**
     * @test
     */
    public function shouldReturnCorrectDataFromApiResult()
    {
        $data = (array) json_decode('[{"id":103801852,"dataReferencia":"2020-05-01","municipio":{"codigoIBGE":"1200013","nomeIBGE":"ACRELÂNDIA","codigoRegiao":"1","nomeRegiao":"NORTE","pais":"BRASIL","uf":{"sigla":"AC","nome":"ACRE"}},"tipo":{"id":6,"descricao":"Auxílio Emergencial","descricaoDetalhada":"Auxílio Emergencial"},"valor":3734400.00,"quantidadeBeneficiados":5379}]');

        $tested = BPCDTO::fromAPI($data);
        $this->assertInstanceOf(BPC::class, $tested);

        $data = (array) $data[0];

        $this->assertValuesFromAuxilioAPI($data, $tested);
    }
}