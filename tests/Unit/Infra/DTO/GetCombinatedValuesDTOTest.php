<?php

namespace Intec\TransparenciaViagensServico\Test\Unit\Infra\DTO;

use DateTimeImmutable;
use Intec\TransparenciaViagensServico\Domain\AuxilioEmergencial\AuxilioEmergencial;
use Intec\TransparenciaViagensServico\Domain\BPC\BPC;
use Intec\TransparenciaViagensServico\Infra\DTO\GetCombinatedValuesDTO;
use Intec\TransparenciaViagensServico\Test\Unit\Domain\Municipio\MunicipioBuilder;
use PHPUnit\Framework\TestCase;

class GetCombinatedValuesDTOTest extends TestCase
{
    /**
     * @test
     */
    public function shouldReturnCorrectValues()
    {
        $expected = [
            "municipio" => [
                "codigoIBGE" => "1234",
                "nomeIBGE" => "1234",
                "codigoRegiao" => "1234",
                "nomeRegiao" => "1234",
                "pais" => "BRASIL",
                "uf" => [
                    "sigla" => "SP",
                    "nome" => "SÃO PAULO"
                ]
            ],
            "dataReferencia" => "2020-05-13",
            "valorTotal" => 2000,
            "quantidadeBeneficiadosTotal" => 100,
            "auxilio" => [
                "valor" => 1000,
                "quantidadeBeneficiados" => 50
            ],
            "bpc" => [
                "valor" => 1000,
                "quantidadeBeneficiados" => 50
            ]
        ];

        $municipio = MunicipioBuilder::aInstance()->build();

        $auxilio = new AuxilioEmergencial($municipio, 1000, 50, new DateTimeImmutable("2020-05-13"));

        $bpc = new BPC($municipio, 1000, 50, new DateTimeImmutable("2020-05-13"));

        $tested = GetCombinatedValuesDTO::toResponse($auxilio, $bpc);

        $this->assertIsArray($tested);
        $this->assertEquals($expected, $tested);
    }
}