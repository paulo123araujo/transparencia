up:
	docker-compose up -d
down:
	docker-compose down
php:
	docker exec -it tvas-php bash
db:
	docker exec -it tvas-sql bash -c "mysql -u tvas -p'tvas' tvas"
migrate:
	docker exec -it tvas-php ./vendor/bin/phinx migrate
log:
	docker exec -it tvas-php bash -c "grc -c conf.log tail -f var/logs/app.log"
install:
	docker exec -it tvas-php composer install
update:
	docker exec -it tvas-php composer update
ana:
	docker exec -it tvas-php vendor/bin/psalm --show-info=true
tests-unit:
	docker exec -it tvas-php vendor/bin/phpunit --testsuite u