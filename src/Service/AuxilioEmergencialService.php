<?php

namespace Intec\TransparenciaViagensServico\Service;

use DateTimeInterface;
use GuzzleHttp\Client;
use Intec\TransparenciaViagensServico\Domain\AuxilioEmergencial\AuxilioEmergencial;
use Intec\TransparenciaViagensServico\Infra\DTO\AuxilioEmergencialDTO;
use Intec\TransparenciaViagensServico\Infra\Repository\PDO\PDORepository;

use function DI\value;

class AuxilioEmergencialService
{
    public const AUXILIO_API_URL = "/api-de-dados/auxilio-emergencial-por-municipio";

    public function __construct(
        private Client $client,
        private PDORepository $repo,
    )
    {
    }

    public function getAuxilioEmergencialPorMunicipio(
        string $municipio,
        DateTimeInterface $anoMes
    ): AuxilioEmergencial|null
    {
        $auxDatabase = $this->repo->getAuxilioEmergencialPorMunicipioEMes($municipio, $anoMes);

        if (!is_null($auxDatabase)) {
            return $auxDatabase;
        }

        $auxilio = $this->getAuxilioEmergencialApi($municipio, $anoMes);
 
        if (is_null($auxilio)) {
            return null;
        }
        
        $this->repo->saveAuxilioEmergencial($auxilio);

        return $auxilio;
    }

    private function getAuxilioEmergencialApi(
        string $municipio,
        DateTimeInterface $anoMes
    ): AuxilioEmergencial|null
    {
        $data = $this->client->get(self::AUXILIO_API_URL, [
            'query' => [
                "codigoIbge" => $municipio,
                "mesAno" => $anoMes->format("Ym"),
                "pagina" => 1
            ]
        ]);

        $response = json_decode((string) $data->getBody());

        if (empty($response)) {
            return null;
        }

        return AuxilioEmergencialDTO::fromAPI($response);
    }
}