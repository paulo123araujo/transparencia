<?php

namespace Intec\TransparenciaViagensServico\Service;

use DateTimeInterface;
use GuzzleHttp\Client;
use Intec\TransparenciaViagensServico\Domain\BPC\BPC;
use Intec\TransparenciaViagensServico\Infra\DTO\BPCDTO;
use Intec\TransparenciaViagensServico\Infra\Repository\PDO\PDORepository;

class BPCService
{
    public const BPC_API_URL = "/api-de-dados/bpc-por-municipio";

    public function __construct(
        private Client $client,
        private PDORepository $repo,
    )
    {
    }

    public function getBPCPorMunicipio(
        string $municipio,
        DateTimeInterface $anoMes
    ): BPC|null
    {
        $bpcDatabase = $this->repo->getBPCPorMunicipioEMesAno($municipio, $anoMes);

        if (!is_null($bpcDatabase)) {
            return $bpcDatabase;
        }

        $bpc = $this->getBPCApi($municipio, $anoMes);

        if (is_null($bpc)) {
            return null;
        }

        $this->repo->saveBPC($bpc);

        return $bpc;
    }

    private function getBPCApi(
        string $municipio,
        DateTimeInterface $anoMes
    ): BPC|null
    {
        $data = $this->client->get(self::BPC_API_URL, [
            'query' => [
                "codigoIbge" => $municipio,
                "mesAno" => $anoMes->format("Ym"),
                "pagina" => 1
            ]
        ]);

        $response = json_decode((string) $data->getBody());

        if (empty($response)) {
            return null;
        }

        return BPCDTO::fromAPI($response);
    }
}