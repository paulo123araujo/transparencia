<?php

namespace Intec\TransparenciaViagensServico\Action;

use DateTimeImmutable;
use Intec\IntecSlimBase\Action\JsonAction;
use Intec\TransparenciaViagensServico\Infra\DTO\GetCombinatedValuesDTO;
use Intec\TransparenciaViagensServico\Service\{
    AuxilioEmergencialService,
    BPCService
};
use Psr\Http\Message\{
    ResponseInterface,
    ServerRequestInterface
};

class GetCombinatedValues extends JsonAction
{
    public function __construct(
        private AuxilioEmergencialService $auxService,
        private BPCService $bpcService
    )
    {
    }

    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface {
        $params = $request->getQueryParams();

        $codigoIBGE = $params["codigoIBGE"];
        $ano = substr($params["anoMes"], 0, 4);
        $mes = substr($params["anoMes"], 4, 2);
        $data = sprintf("%s-%s", $ano, $mes);
        $data = new DateTimeImmutable($data);

        if (!isset($codigoIBGE) || !isset($params["anoMes"])) {
            return $this->toJson(
                $response,
                404,
                "É necessário enviar o codigoIBGE e o AnoMês"
            );
        }

        $aux = $this->auxService->getAuxilioEmergencialPorMunicipio($codigoIBGE, $data);

        $bpc = $this->bpcService->getBPCPorMunicipio($codigoIBGE, $data);

        if (is_null($aux) && is_null($bpc)) {
            return $this->toJson(response: $response, message: "Não há dados na data escolhida.");
        }

        $combinated = GetCombinatedValuesDTO::toResponse($aux, $bpc);

        return $this->toJson(response: $response, data: $combinated);
    }
}