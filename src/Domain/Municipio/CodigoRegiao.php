<?php

namespace Intec\TransparenciaViagensServico\Domain\Municipio;

class CodigoRegiao
{
    private function __construct(
        private string $codigoRegiao
    )
    {
    }

    public static function new(string $codigoRegiao): CodigoRegiao
    {
        return new self($codigoRegiao);
    }

    public function codigoRegiao(): string
    {
        return $this->codigoRegiao;
    }
}