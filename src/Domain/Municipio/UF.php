<?php

namespace Intec\TransparenciaViagensServico\Domain\Municipio;

use DomainException;
use Exception;

class UF
{
    private function __construct(
        private string $sigla,
        private string $nome
    )
    {
        $this->validate($sigla, $nome);
    }

    public static function new(string $sigla, string $nome): UF
    {
        return new self($sigla, $nome);
    }

    public function sigla(): string
    {
        return $this->sigla;
    }

    public function nome(): string
    {
        return $this->nome;
    }

    /**
     * @throw DomainException
     */
    private function validate(string $sigla, string $nome): void
    {
        $assocStates = $this->allBrazilianStatesWithInitials();
        if (!isset($assocStates[$sigla])) {
            throw new DomainException("Estado inválido.");
        }

        if ($nome !== $assocStates[$sigla]) {
            throw new DomainException("Estado inválido.");
        }
    }

    /**
     * @return array<string,string>
     */
    private function allBrazilianStatesWithInitials(): array
    {
        return [
            "AC" => "ACRE",
            "AL" => "ALAGOAS",
            "AP" => "AMAPÁ",
            "AM" => "AMAZONAS",
            "BA" => "BAHIA",
            "CE" => "CEARÁ",
            "ES" => "ESPIRÍTO SANTO",
            "GO" => "GOIÁS",
            "MA" => "MARANHÃO",
            "MT" => "MATO GROSSO",
            "MS" => "MATO GROSSO DO SUL",
            "MG" => "MINAS GERAIS",
            "PA" => "PARÁ",
            "PB" => "PARAÍBA",
            "PR" => "PARANÁ",
            "PE" => "PERNAMBUCO",
            "PI" => "PIAUÍ",
            "RJ" => "RIO DE JANEIRO",
            "RN" => "RIO GRANDE DO NORTE",
            "RS" => "RIO GRANDE DO SUL",
            "RO" => "RONDÔNIA",
            "RR" => "RORAIMA",
            "SC" => "SANTA CATARINA",
            "SP" => "SÃO PAULO",
            "SE" => "SERGIPE",
            "TO" => "TOCANTINS",
            "DF" => "DISTRITO FEDERAL"
        ];
    }
}