<?php

namespace Intec\TransparenciaViagensServico\Domain\Municipio;

class NomeIBGE
{
    private function __construct(
        private string $nomeIBGE
    )
    {
    }

    public static function new(string $nomeIBGE): NomeIBGE
    {
        return new self($nomeIBGE);
    }

    public function nomeIBGE(): string
    {
        return $this->nomeIBGE;
    }
}