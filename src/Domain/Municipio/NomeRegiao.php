<?php

namespace Intec\TransparenciaViagensServico\Domain\Municipio;

class NomeRegiao
{
    private function __construct(
        private string $nomeRegiao
    )
    {
    }

    public static function new(string $nomeRegiao): NomeRegiao
    {
        return new self($nomeRegiao);
    }

    public function nomeRegiao(): string
    {
        return $this->nomeRegiao;
    }
}