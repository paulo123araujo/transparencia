<?php

namespace Intec\TransparenciaViagensServico\Domain\Municipio;

class Municipio
{
    public function __construct(
        private CodigoIBGE $codigoIBGE,
        private NomeIBGE $nomeIBGE,
        private CodigoRegiao $codigoRegiao,
        private NomeRegiao $nomeRegiao,
        private Pais $pais,
        private UF $uf,
        private int|null $id
    )
    {
    }

    public function id(): int|null
    {
        return $this->id;
    }

    public function codigoIBGE(): CodigoIBGE
    {
        return $this->codigoIBGE;
    }

    public function nomeIBGE(): NomeIBGE
    {
        return $this->nomeIBGE;
    }

    public function codigoRegiao(): CodigoRegiao
    {
        return $this->codigoRegiao;
    }

    public function nomeRegiao(): NomeRegiao
    {
        return $this->nomeRegiao;
    }

    public function pais(): Pais
    {
        return $this->pais;
    }

    public function uf(): UF
    {
        return $this->uf;
    }

    public function isEquals(Municipio $outroMunicipio): bool
    {
        return 
            $this->codigoIBGE()->codigoIBGE() === $outroMunicipio->codigoIBGE()->codigoIBGE() &&
            $this->id() === $outroMunicipio->id();
    }
}