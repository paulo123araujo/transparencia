<?php

namespace Intec\TransparenciaViagensServico\Domain\Municipio;

class CodigoIBGE
{
    private function __construct(
        private string $codigoIBGE
    )
    {
    }

    public static function new(string $codigoIBGE): CodigoIBGE
    {
        return new self($codigoIBGE);
    }

    public function codigoIBGE(): string
    {
        return $this->codigoIBGE;
    }
}