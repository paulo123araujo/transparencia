<?php

namespace Intec\TransparenciaViagensServico\Domain\Municipio;

use DomainException;

class Pais
{
    private function __construct(
        private string $pais
    )
    {
        $this->validate($pais);
    }

    public static function new(string $pais): Pais
    {
        return new self($pais);
    }

    public function pais(): string
    {
        return $this->pais;
    }

    /**
     * @throw DomainException
     */
    private function validate(string $pais): void
    {
        if ($pais !== "BRASIL") {
            throw new DomainException("O país precisa ser o Brasil.");
        }
    }
}