<?php

namespace Intec\TransparenciaViagensServico\Domain\BPC;

use DateTimeInterface;
use Intec\TransparenciaViagensServico\Domain\Municipio\Municipio;

class BPC
{
    public function __construct(
        private Municipio $municipio,
        private float $valor,
        private int $quantidadeBeneficiados,
        private DateTimeInterface $dataReferencia
    )
    {
    }

    public function municipio(): Municipio
    {
        return $this->municipio;
    }

    public function valor(): float
    {
        return $this->valor;
    }

    public function quantidadeBeneficiados(): int
    {
        return $this->quantidadeBeneficiados;
    }

    public function dataReferencia(): DateTimeInterface
    {
        return $this->dataReferencia;
    }
}