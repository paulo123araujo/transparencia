<?php

namespace Intec\TransparenciaViagensServico\Domain\BPC;

use DateTimeInterface;
use Intec\TransparenciaViagensServico\Domain\Municipio\{
    CodigoIBGE,
    CodigoRegiao,
    Municipio,
    NomeIBGE,
    NomeRegiao,
    Pais,
    UF
};

class BPCFactory
{
    public static function create(
        string $codigoIBGE,
        string $codigoRegiao,
        string $nomeIBGE,
        string $nomeRegiao,
        string $pais,
        string $ufSigla,
        string $ufNome,
        float $valor,
        int $numeroBeneficiados,
        DateTimeInterface $dataReferencia,
        int|null $municipioId = null
    ): BPC {
        return new BPC(
            new Municipio(
                CodigoIBGE::new($codigoIBGE),
                NomeIBGE::new($nomeIBGE),
                CodigoRegiao::new($codigoRegiao),
                NomeRegiao::new($nomeRegiao),
                Pais::new($pais),
                UF::new($ufSigla, $ufNome),
                $municipioId
            ),
            $valor,
            $numeroBeneficiados,
            $dataReferencia
        );
    }
}
