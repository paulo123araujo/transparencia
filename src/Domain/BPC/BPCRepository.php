<?php

namespace Intec\TransparenciaViagensServico\Domain\BPC;

use DateTimeInterface;

interface BPCRepository
{
    public function getBPCPorMunicipioEMesAno(
        string $codigoIBGE,
        DateTimeInterface $data
    ): BPC|null;

    public function saveBPC(BPC $bpc): void;
}