<?php

namespace Intec\TransparenciaViagensServico\Domain\AuxilioEmergencial;

use DateTimeInterface;

interface AuxilioEmergencialRepository
{
    public function getAuxilioEmergencialPorMunicipioEMes(
        string $codigoIBGE,
        DateTimeInterface $anoMes
    ): AuxilioEmergencial|null;

    public function saveAuxilioEmergencial(AuxilioEmergencial $auxilio): void;
}
