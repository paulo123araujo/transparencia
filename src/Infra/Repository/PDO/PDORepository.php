<?php

namespace Intec\TransparenciaViagensServico\Infra\Repository\PDO;

use DateTimeInterface;
use Intec\TransparenciaViagensServico\Domain\AuxilioEmergencial\{
    AuxilioEmergencialRepository,
    AuxilioEmergencial
};
use Intec\TransparenciaViagensServico\Domain\BPC\{
    BPCRepository,
    BPC
};
use Intec\TransparenciaViagensServico\Domain\Municipio\Municipio;
use Intec\TransparenciaViagensServico\Infra\DTO\{
    AuxilioEmergencialDTO,
    BPCDTO,
    MunicipioDTO
};
use PDO;
use PDOException;

class PDORepository implements AuxilioEmergencialRepository, BPCRepository
{
    public function __construct(private PDO $pdo)
    {
    }

    public function saveAuxilioEmergencial(AuxilioEmergencial $auxilio): void
    {
        $codigoIBGE = $auxilio->municipio()->codigoIBGE()->codigoIBGE();
        $municipio = $this->getMunicipio($codigoIBGE);

        if (is_null($municipio)) {
            $municipio = $this->saveMunicipio($auxilio->municipio());
        }

        $stmt = $this->pdo->prepare("
            INSERT INTO auxilios_emergenciais(valor, quantidadeBeneficiados, dataReferencia, municipio_id)
            VALUES (:valor, :quantidadeBeneficiados, :dataReferencia, :municipio_id)
        ");

        $stmt->bindValue(":valor", $auxilio->valor());
        $stmt->bindValue(":quantidadeBeneficiados", $auxilio->quantidadeBeneficiados());
        $stmt->bindValue(":dataReferencia", $auxilio->dataReferencia()->format("Y-m-d"));
        $stmt->bindValue(":municipio_id", $municipio->id());

        try {
            $stmt->execute();
        } catch (PDOException $e) {
        }
    }

    public function saveBPC(BPC $bpc): void
    {
        $codigoIBGE = $bpc->municipio()->codigoIBGE()->codigoIBGE();
        $municipio = $this->getMunicipio($codigoIBGE);

        if (is_null($municipio)) {
            $municipio = $this->saveMunicipio($bpc->municipio());
        }

        $stmt = $this->pdo->prepare("
            INSERT INTO bpcs(valor, quantidadeBeneficiados, dataReferencia, municipio_id)
            VALUES (:valor, :quantidadeBeneficiados, :dataReferencia, :municipio_id)
        ");

        $stmt->bindValue(":valor", $bpc->valor());
        $stmt->bindValue(":quantidadeBeneficiados", $bpc->quantidadeBeneficiados());
        $stmt->bindValue(":dataReferencia", $bpc->dataReferencia()->format("Y-m-d"));
        $stmt->bindValue(":municipio_id", $municipio->id());

        $stmt->execute();
    }

    public function saveMunicipio(Municipio $municipio): Municipio|null
    {
        $stmt = $this->pdo->prepare("
            INSERT INTO municipios
            (codigoIBGE, nomeIBGE, codigoRegiao, nomeRegiao, pais, ufSigla, ufNome)
            VALUES (
                :codigoIBGE, :nomeIBGE, :codigoRegiao, :nomeRegiao, :pais, :ufSigla, :ufNome
            )
        ");

        $stmt->bindParam(":codigoIBGE", $municipio->codigoIBGE()->codigoIBGE());
        $stmt->bindParam(":nomeIBGE", $municipio->nomeIBGE()->nomeIBGE());
        $stmt->bindParam(":codigoRegiao", $municipio->codigoRegiao()->codigoRegiao());
        $stmt->bindParam(":nomeRegiao", $municipio->nomeRegiao()->nomeRegiao());
        $stmt->bindParam(":pais", $municipio->pais()->pais());
        $stmt->bindParam(":ufSigla", $municipio->uf()->sigla());
        $stmt->bindParam(":ufNome", $municipio->uf()->nome());

        try {
            $stmt->execute();
        } catch(PDOException $e) {
        }
        
        return $this->getMunicipio($municipio->codigoIBGE()->codigoIBGE());
    }

    public function getMunicipio(string $codigoIBGE): Municipio|null
    {
        $stmt = $this->pdo->prepare("
            SELECT * FROM municipios WHERE codigoIBGE = :codigoIBGE LIMIT 1
        ");

        $stmt->execute([":codigoIBGE" => $codigoIBGE]);

        $result = $stmt->fetch(PDO::FETCH_ASSOC);

        if (empty($result)) {
            return null;
        }

        return MunicipioDTO::fromDatabase($result);
    }

    public function getAuxilioEmergencialPorMunicipioEMes(
        string $codigoIBGE,
        DateTimeInterface $anoMes
    ): AuxilioEmergencial|null
    {
        $prepare = $this->pdo->prepare("
            SELECT 
                aux.*,
                m.id as municipio_id,
                m.codigoIBGE,
                m.codigoRegiao,
                m.nomeIBGE,
                m.nomeRegiao,
                m.pais,
                m.ufSigla,
                m.ufNome
            FROM auxilios_emergenciais AS aux
            INNER JOIN municipios AS m ON (m.id = aux.municipio_id)
            WHERE
                MONTH(aux.dataReferencia) = ? AND
                YEAR(aux.dataReferencia) = ? AND
                m.codigoIBGE = ?
        ");

        $prepare->execute([
            $anoMes->format("m"),
            $anoMes->format("Y"),
            $codigoIBGE
        ]);

        $result = $prepare->fetch(PDO::FETCH_ASSOC);

        if (empty($result)) {
            return null;
        }

        return AuxilioEmergencialDTO::fromDatabase($result);
    }

    public function getBPCPorMunicipioEMesAno(
        string $codigoIBGE,
        DateTimeInterface $data
    ): BPC|null
    {
        $prepare = $this->pdo->prepare("
            SELECT
                bpcs.*,
                m.id as municipio_id,
                m.codigoIBGE,
                m.codigoRegiao,
                m.nomeIBGE,
                m.nomeRegiao,
                m.pais,
                m.ufSigla,
                m.ufNome
            FROM bpcs
            INNER JOIN municipios AS m ON (m.id = bpcs.municipio_id)
            WHERE
                MONTH(bpcs.dataReferencia) = ? AND
                YEAR(bpcs.dataReferencia) = ? AND
                m.codigoIBGE = ?
        ");

        try {
            $prepare->execute([
                $data->format("m"),
                $data->format("Y"),
                $codigoIBGE
            ]);
        } catch(PDOException $e) {
        }
        

        $result = $prepare->fetch(PDO::FETCH_ASSOC);

        if (empty($result)) {
            return null;
        }

        return BPCDTO::fromDatabase($result);
    }
}