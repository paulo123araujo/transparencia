<?php

namespace Intec\TransparenciaViagensServico\Infra\DTO;

use Intec\TransparenciaViagensServico\Domain\Municipio\CodigoIBGE;
use Intec\TransparenciaViagensServico\Domain\Municipio\CodigoRegiao;
use Intec\TransparenciaViagensServico\Domain\Municipio\Municipio;
use Intec\TransparenciaViagensServico\Domain\Municipio\NomeIBGE;
use Intec\TransparenciaViagensServico\Domain\Municipio\NomeRegiao;
use Intec\TransparenciaViagensServico\Domain\Municipio\Pais;
use Intec\TransparenciaViagensServico\Domain\Municipio\UF;

class MunicipioDTO
{
    public static function fromDatabase(array $data): Municipio
    {
        return new Municipio(
            CodigoIBGE::new($data["codigoIBGE"]),
            NomeIBGE::new($data["nomeIBGE"]),
            CodigoRegiao::new($data["codigoRegiao"]),
            NomeRegiao::new($data["nomeRegiao"]),
            Pais::new($data["pais"]),
            UF::new($data["ufSigla"], $data["ufNome"]),
            $data["id"]
        );
    }
}