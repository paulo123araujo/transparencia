<?php

namespace Intec\TransparenciaViagensServico\Infra\DTO;

use DateTimeImmutable;
use Intec\TransparenciaViagensServico\Domain\BPC\{
    BPC,
    BPCFactory
};

class BPCDTO
{
    public static function fromDatabase(array $auxilio): BPC
    {
        return BPCFactory::create(
            $auxilio["codigoIBGE"],
            $auxilio["codigoRegiao"],
            $auxilio["nomeIBGE"],
            $auxilio["nomeRegiao"],
            $auxilio["pais"],
            $auxilio["ufSigla"],
            $auxilio["ufNome"],
            $auxilio["valor"],
            $auxilio["quantidadeBeneficiados"],
            new DateTimeImmutable($auxilio["dataReferencia"]),
            $auxilio["municipio_id"]
        );
    }

    public static function fromAPI(array $data): BPC
    {
        $data = $data[0];
        return BPCFactory::create(
            $data->municipio->codigoIBGE,
            $data->municipio->codigoRegiao,
            $data->municipio->nomeIBGE,
            $data->municipio->nomeRegiao,
            $data->municipio->pais,
            $data->municipio->uf->sigla,
            $data->municipio->uf->nome,
            $data->valor,
            $data->quantidadeBeneficiados,
            new DateTimeImmutable($data->dataReferencia)
        );
    }
}
