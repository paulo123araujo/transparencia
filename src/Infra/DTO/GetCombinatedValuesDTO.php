<?php

namespace Intec\TransparenciaViagensServico\Infra\DTO;

use Intec\TransparenciaViagensServico\Domain\AuxilioEmergencial\AuxilioEmergencial;
use Intec\TransparenciaViagensServico\Domain\BPC\BPC;

class GetCombinatedValuesDTO
{
    public static function toResponse(AuxilioEmergencial|null $auxilio, BPC|null $bpc): array
    {
        $municipio = is_null($auxilio) ? $bpc->municipio() : $auxilio->municipio();
        $dataReferencia = is_null($auxilio) ? $bpc->dataReferencia() : $auxilio->dataReferencia();
        return [
            "municipio" => [
                "codigoIBGE" => $municipio->codigoIBGE()->codigoIBGE(),
                "nomeIBGE" => $municipio->nomeIBGE()->nomeIBGE(),
                "codigoRegiao" => $municipio->codigoRegiao()->codigoRegiao(),
                "nomeRegiao" => $municipio->nomeRegiao()->nomeRegiao(),
                "pais" => $municipio->pais()->pais(),
                "uf" => [
                    "sigla" => $municipio->uf()->sigla(),
                    "nome" => $municipio->uf()->nome()
                ]
            ],
            "dataReferencia" => $dataReferencia->format("Y-m-d"),
            "valorTotal" => ($auxilio->valor() ?: 0) + ($bpc->valor() ?: 0),
            "quantidadeBeneficiadosTotal" => ($auxilio->quantidadeBeneficiados() ?: 0) + ($bpc->quantidadeBeneficiados() ?: 0),
            "auxilio" => [
                "valor" => $auxilio->valor() ?: 0,
                "quantidadeBeneficiados" => $auxilio->quantidadeBeneficiados() ?: 0
            ],
            "bpc" => [
                "valor" => $bpc->valor() ?: 0,
                "quantidadeBeneficiados" => $bpc->quantidadeBeneficiados() ?: 0
            ]
        ];
    }
}